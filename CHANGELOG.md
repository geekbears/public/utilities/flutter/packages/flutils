## [3.0.0] - 09/11/22
* Renamed package to `gb_flutils`

## [2.0.1] - 09/11/22
* Added pairedList transformation utils

## [2.0.0] - 25/05/22
* MAJOR/BREAKING CHANGE
  * Moved major code to other packages

## [1.4.0] - 11/05/22
* Updated `auth_http_client` dependency

## [1.3.3] - 12/04/22
* Added [original] prop to:
  * ServerException
  * ServerFailure

## [1.3.2] - 07/04/22
* Update user entity and model

## [1.3.1] - 05/04/22
* Fix type casting in ServerException 
  * List<String>.from(error["messages"])

## [1.3.0] - 05/04/22
* Rename  UnknowFailure -> UnknownFailure (typo) **Brake Changes**
* Make ServerExceptions and ServerFailures to support array of messages and Stack if available

## [1.2.9] - 01/04/22
* Fixed double symbol on [formattedPrice] function

## [1.2.8] - 01/04/22
* Added some number to string utils
* Added some  string utils

## [1.2.7] - 01/02/22
* Added new option to `DateTimeSerializerPlugin` to return nulls when invalid Date

## [1.2.6] - 01/02/22
* Added toString method to LoadedData class

## [1.2.5] - 17/01/22
* Added ListenableBloc class

## [1.2.4] - 06/01/22
* Added missing methods in http_client class

## [1.2.3] - 30/12/21
* Fix on LoadedData class

## [1.2.2] - 30/12/21
* Added const string version of environment
* Fixes in user model

## [1.2.1] - 30/12/21
* Added LoadedData<T> class in favor of CacheableItem<T> class

## [1.2.0] - 15/12/21
* Breaking changes
  * Some refactor in HttpClientServiceMock, now it supports headers

## [1.1.11] - 04/12/21
* Added BuiltNumToStringPlugin
 
## [1.1.10] - 04/12/21
* Added built_bool_num_parse plugin

## [1.1.9] - 03/12/21
* Updated User class

## [1.1.8] - 23/11/21
* export AuthHttpClient constants

## [1.1.7] - 26/10/21
* Added useful stubs for httpclient service

## [1.1.6] - 25/10/21
* Make mock services allow nulls
  
## [1.1.5] - 25/10/21
* Added HttpClientService & SharedPreferences Mocks

## [1.1.4] - 15/10/21
* Some dependency changes and improves in ClientServiceImpl
  
## [1.1.3] - 15/10/21
* Export user model

## [1.1.2] - 13/10/21
* Added AppEnvironment enum type helper

## [1.1.1] - 13/10/21
* Fix exports

## [1.1.0] - 13/10/21
* Refactor structure
* Added user model

## [1.0.3] - 18/08/21
* Fix EnvironmentProvider widget

## [1.0.2] - 18/08/21
* Updated built value files

## [1.0.1] - 11/08/21
* Null check changes

## [1.0.0] - 06/08/21
* Null Safety

## [0.0.5] - 05/03/21

* Compatible with `built_value: ^8.0.0`

## [0.0.4] - 30/07/20

* Update dependencies

## [0.0.3] - 21/07/20

* Added "encoding" flags to `DateTimeSerializerPlugin` , now it also applies a custom encoding to ISO String

## [0.0.2] - 21/05/20

* Added Built Value Plugin Helpers

## [0.0.1] - 23/03/20

* Initial release
