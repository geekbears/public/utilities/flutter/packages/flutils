import 'package:collection/collection.dart';

/// Given an array of sequential paired items this will transform ir into a map
/// ie:
/// ["key1", "value1", "key2", "value2"] -> {"key1": "value1", "key2": "value2"}
Map<String, dynamic> pairedListToMap(List<dynamic> list) {
  Map<String, dynamic> mapObject = {};
  for (var i = 0; i < list.length; i++) {
    if (i % 2 == 0) {
      mapObject.addEntries([
        MapEntry(list[i], list[i + 1]),
      ]);
    }
  }
  return mapObject;
}

/// Given  a map this will transform ir into an array of sequential paired items
/// ie:
/// {"key1": "value1", "key2": "value2"} -> ["key1", "value1", "key2", "value2"]
List<dynamic> mapToPairedList(Map<dynamic, dynamic> map) {
  List<dynamic> list = map.map((key, value) => MapEntry(key, [key, value])).values.flattened.toList();
  return list;
}

/// This function let you modify an array of sequential paired items as a map and return it in the array form
/// Check [pairedListToMap] for more details
List<dynamic> modifyPairedListAsMap(
    List<dynamic> list, Map<dynamic, dynamic> Function(Map<dynamic, dynamic>) transformer) {
  return mapToPairedList(
    transformer(
      pairedListToMap(list),
    ),
  );
}
