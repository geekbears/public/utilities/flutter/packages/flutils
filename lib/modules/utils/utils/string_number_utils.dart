/// Given a number string this will display decimals just if it contains them
/// For example
/// 0.012 => 0.012
/// 3.2 => 3.2
/// 2.0 => 2
String doubleStringWithoutTrailingZeros(String? doubleString) {
  return (doubleString ?? "0.00").split(".").reduce((value, element) {
    return "$value${double.parse(element) > 0 ? ".$element" : ""}";
  });
}

/// Given an object this function will try to parse as double and round the value
/// returning a string
/// It also provides a fallback function [orElse] to use if the value processed is an invalid number
/// by default this returns `0` if no fallback provided
String numberToRoundedValue(
  dynamic number, {
  String Function(dynamic)? orElse,
}) {
  if (number is String) {
    return double.parse(number).round().toString();
  } else if (number is double) {
    return number.round().toString();
  } else if (number is int) {
    return number.toString();
  }
  return orElse?.call(number) ?? "0";
}

/// Returns a string representing a formatted price with currency
/// A custom [symbol] can be provided to be used instead
String formattedPrice(double? price, [String? symbol]) {
  if (price == null) return "0";
  return "${symbol ?? "\$"}${price.toStringAsFixed(2)}";
}

/// Check if given string is empty or null
bool isStrNil(String? string) {
  return string == null || string.isEmpty || string.trim().isEmpty;
}
