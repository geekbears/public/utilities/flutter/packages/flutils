String and Transformation Utilities
===================================

This package provides utility functions for string manipulation, number formatting, and data transformation. The utilities are grouped into three separate files:

1. `string_number_utils.dart`
2. `string_utils.dart`
3. `transformations_utils.dart`

---


## string\_number\_utils.dart
--------------------------

### doubleStringWithoutTrailingZeros

```
dartCopy codeString doubleStringWithoutTrailingZeros(String? doubleString)

```

Given a number string, this function will display decimals only if it contains them. For example:

- `0.012` =&gt; `0.012`
- `3.2` =&gt; `3.2`
- `2.0` =&gt; `2`

### numberToRoundedValue

```
dartCopy codeString numberToRoundedValue(dynamic number, {String Function(dynamic)? orElse})

```

Given an object, this function will try to parse it as a double and round the value, returning a string. It also provides a fallback function `[orElse]` to use if the value processed is an invalid number. By default, this returns `0` if no fallback is provided.

### formattedPrice

```
dartCopy codeString formattedPrice(double? price, [String? symbol])

```

Returns a string representing a formatted price with currency. A custom `[symbol]` can be provided to be used instead.

### isStrNil

```
dartCopy codebool isStrNil(String? string)

```

Checks if the given string is empty or null.

---

## string\_utils.dart
------------------

### convertToTitleCase

```
dartCopy codeString convertToTitleCase(String text)

```

Returns a title case form of the given string. It capitalizes the first letter of each word.

### StringUtilsExtension

```
dartCopy codeextension StringUtilsExtension on String {
  String toTitleCase()
}

```

An extension method for `String` class which returns a title case form of the given string.

---

### transformations\_utils.dart
---------------------------

### pairedListToMap

```
dartCopy codeMap pairedListToMap(List list)

```

Given an array of sequential paired items, this function will transform it into a map. For example:

- `["key1", "value1", "key2", "value2"]` -&gt; `{"key1": "value1", "key2": "value2"}`

### mapToPairedList

```
dartCopy codeList mapToPairedList(Map map)

```

Given a map, this function will transform it into an array of sequential paired items. For example:

- `{"key1": "value1", "key2": "value2"}` -&gt; `["key1", "value1", "key2", "value2"]`

### modifyPairedListAsMap

```
dartCopy codeList modifyPairedListAsMap(
    List list, Map Function(Map) transformer)

```

This function lets you modify an array of sequential paired items as a map and return it in the array form. Check `[pairedListToMap]` for more details.

